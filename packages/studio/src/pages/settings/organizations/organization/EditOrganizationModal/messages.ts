import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  name: 'Name',
  nameDescription: 'The display name of the organization',
  logo: 'Logo',
  logoDescription: 'The logo that represents this organization',
  noFile: 'No logo selected…',
  edit: 'Edit Organization',
  cancel: 'Cancel',
  submit: 'Submit',
});
