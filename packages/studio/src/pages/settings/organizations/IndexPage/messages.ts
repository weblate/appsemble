import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: 'Organizations',
  createButton: 'Create organization',
  organizationName: 'Organization name',
  organizationId: 'Organization ID',
  cancelLabel: 'Cancel',
  logo: 'Logo',
});
