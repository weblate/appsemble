import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  details: 'Details',
  icon: 'Icon',
});
