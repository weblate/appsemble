import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  appLogo: 'App logo',
  cancel: 'Cancel',
  clone: 'Clone App',
  name: 'Name',
  nameDescription: 'The name of the app',
  description: 'Description',
  descriptionDescription: 'A short description of what the app can do.',
  private: 'Private',
  privateDescription: 'Hide this app from the public app list',
  organization: 'Organization',
  submit: 'Create',
  view: 'View App',
  resources: 'Resources',
  resourcesDescription: 'Copy any resources that are allowed to be cloned.',
  readMore: 'Read more',
  readLess: 'Read less',
});
