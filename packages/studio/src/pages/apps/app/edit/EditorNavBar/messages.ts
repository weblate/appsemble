import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  preview: 'Preview',
  publish: 'Publish',
  viewLive: 'View live',
  recipe: 'Recipe',
  coreStyle: 'Core style',
  sharedStyle: 'Shared style',
  switchManual: 'Manual editor',
  switchGUI: 'GUI Editor',
});
