import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  appStore: 'App store',
  blockStore: 'Block store',
  documentation: 'Documentation',
});
