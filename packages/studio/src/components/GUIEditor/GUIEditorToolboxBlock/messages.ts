import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  blockLogo: '{name} Logo',
  search: 'Search Blocks',
  enterBlockName: 'Enter a block name',
});
