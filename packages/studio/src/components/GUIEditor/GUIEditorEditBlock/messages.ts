import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  noParameters: '{name} has no editable parameters',
  noActions: '{name} has no linkable actions',
  parameters: 'Parameters',
  actions: 'Actions',
  events: 'Events',
  error: 'There was a problem loading block: “{blockName}”',
});
