import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: 'Add Block',
  moreInfo: 'More Info',
  error: 'There was a problem loading blocks',
});
