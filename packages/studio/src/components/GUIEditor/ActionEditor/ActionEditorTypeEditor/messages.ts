import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  notSupported:
    'This action can’t be updated, because it is not available in the GUI editor yet. This action can be defined using the manual editor.',
  noAction: 'No action selected.',
});
