import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  empty: 'No page selected',
  toHelp: 'Where to link to. This should be a page name.',
  toLabel: 'To',
});
