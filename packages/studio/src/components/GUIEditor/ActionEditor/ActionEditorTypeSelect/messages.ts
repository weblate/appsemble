import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  empty: 'No action',
  actionType: 'Action Type',
});
