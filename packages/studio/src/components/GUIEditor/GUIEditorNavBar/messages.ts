import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  recipe: 'Recipe',
  addBlock: 'Add Block',
  editBlock: 'Edit',
});
