import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  loginButton: 'Login',
  loginFailed: 'Login failed',
  passwordError: 'A password is required',
  passwordLabel: 'Password',
  usernameError: 'This needs to be a valid email address',
  usernameLabel: 'Email',
});
