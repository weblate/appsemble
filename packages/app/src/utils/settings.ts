export const {
  apiUrl,
  blockManifests,
  definition,
  id: appId,
  languages,
  logins,
  sentryDsn,
  sentryEnvironment,
  vapidPublicKey,
} = window.settings;
delete window.settings;
