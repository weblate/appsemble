import { setFixtureBase, setLogLevel } from './src';

setFixtureBase(__dirname);
setLogLevel(0);
