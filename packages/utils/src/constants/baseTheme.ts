export const baseTheme = {
  primaryColor: '#5291ff',
  linkColor: '#3273dc',
  infoColor: '#209cee',
  successColor: '#23d160',
  warningColor: '#ffdd57',
  dangerColor: '#ff3860',
  splashColor: '#ffffff',
  themeColor: '#ffffff',
  tileLayer: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
};
